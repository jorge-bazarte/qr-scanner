package com.jabm.mlkit.barcode.read.libcodescanner;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.google.zxing.Result;
import com.jabm.mlkit.barcode.R;
import com.jabm.mlkit.barcode.read.ScanResultInterface;

public class QrScannerCsDialogFragment extends AppCompatDialogFragment implements DecodeCallback {

    public static String TAG = "QrScannerDialogFragment";
    private ScanResultInterface scanResultInterface;
    private CodeScanner codeScanner;

    @SuppressLint("ValidFragment")
    private QrScannerCsDialogFragment() {
    }

    public static QrScannerCsDialogFragment newInstance() {
        QrScannerCsDialogFragment f = new QrScannerCsDialogFragment();
        Bundle args = new Bundle();
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_FullScreenDialogStyle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_fragment_qr_scanner_cs, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        CodeScannerView scannerView = view.findViewById(R.id.scanner_view);
        codeScanner = new CodeScanner(getActivity(), scannerView);
        codeScanner.setDecodeCallback(this);
        view.findViewById(R.id.close_dialog).setOnClickListener(v -> dismiss());
        view.findViewById(R.id.button_gallery).setOnClickListener(v -> Toast.makeText(getContext(),"No disponible",Toast.LENGTH_SHORT).show());
        if (isCameraPermissionsGranted()) {
            codeScanner.startPreview();
        } else {
            getCameraPermissions();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        codeScanner.releaseResources();
        super.onPause();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (isCameraPermissionsGranted()) {
            codeScanner.startPreview();
        } else {
            dismiss();
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private boolean isCameraPermissionsGranted() {
        return (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED);
    }

    private void getCameraPermissions() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 1);
    }

    public void addScanResultListener(ScanResultInterface scanResultInterface) {
        this.scanResultInterface = scanResultInterface;
    }

    @Override
    public void onDecoded(@androidx.annotation.NonNull Result result) {
        getActivity().runOnUiThread(() -> {
            if (scanResultInterface != null) {
                dismiss();
                scanResultInterface.onScanResult(result.getText());
            }
        });
    }
}
