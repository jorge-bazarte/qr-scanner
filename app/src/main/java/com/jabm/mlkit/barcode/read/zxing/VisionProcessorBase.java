// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package com.jabm.mlkit.barcode.read.zxing;

import android.graphics.Bitmap;
import android.media.Image;
import android.support.annotation.NonNull;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.LuminanceSource;
import com.google.zxing.NotFoundException;
import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;

import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class VisionProcessorBase<T> implements VisionImageProcessor {

    private final AtomicBoolean shouldThrottle = new AtomicBoolean(false);

    public VisionProcessorBase() {
    }

    @Override
    public void process(ByteBuffer buffer, final FrameMetadata frameMetadata) {
        if (shouldThrottle.get()) {
            return;
        }
        buffer.rewind();
        byte[] data = new byte[buffer.capacity()];
        buffer.get(data);
        PlanarYUVLuminanceSource source = new PlanarYUVLuminanceSource(
                data, frameMetadata.getWidth(), frameMetadata.getHeight(), 0, 0, frameMetadata.getWidth(),
                frameMetadata.getHeight(), false);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
        detectInVisionImage(bitmap);
    }

    @Override
    public void process(Bitmap bitmap) {
        if (shouldThrottle.get()) {
            return;
        }
        int[] intArray = new int[bitmap.getWidth() * bitmap.getHeight()];
        bitmap.getPixels(intArray, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
        LuminanceSource source = new RGBLuminanceSource(bitmap.getWidth(), bitmap.getHeight(), intArray);
        detectInVisionImage(new BinaryBitmap(new HybridBinarizer(source)));
    }

    @Override
    public void process(Image image, int rotation) {
        if (shouldThrottle.get()) {
            return;
        }
        Image.Plane[] planes = image.getPlanes();
        ByteBuffer buffer = planes[0].getBuffer();
        buffer.rewind();
        byte[] data = new byte[buffer.capacity()];
        buffer.get(data);
        PlanarYUVLuminanceSource source = new PlanarYUVLuminanceSource(
                data, image.getWidth(), image.getHeight(), 0, 0, image.getWidth(),
                image.getHeight(), false);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
        detectInVisionImage(bitmap);
    }

    private void detectInVisionImage(BinaryBitmap bitmap) {
        shouldThrottle.set(true);
        try {
            shouldThrottle.set(false);
            VisionProcessorBase.this.onSuccess(detectInImage(bitmap));
        } catch (FormatException | ChecksumException | NotFoundException e) {
            shouldThrottle.set(false);
            VisionProcessorBase.this.onFailure(e);
        }


    }

    @Override
    public void stop() {
    }

    protected abstract Result detectInImage(BinaryBitmap bitmap) throws FormatException, ChecksumException, NotFoundException;

    protected abstract void onSuccess(@NonNull Result results);

    protected abstract void onFailure(@NonNull Exception e);
}
