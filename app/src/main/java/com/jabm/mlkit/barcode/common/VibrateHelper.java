package com.jabm.mlkit.barcode.common;

import android.app.Activity;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;

import static android.content.Context.VIBRATOR_SERVICE;

public class VibrateHelper {
    public static final int SHORT_DURATION = 100;
    public static final int MEDIUM_DURATION = 250;
    public static final int LONG_DURATION = 500;
    private Activity activity;
    private Vibrator vibrator;

    public VibrateHelper(Activity activity) {
        this.vibrator = (Vibrator) activity.getSystemService(VIBRATOR_SERVICE);
    }

    public void vibrate(int duration) {
        if (Build.VERSION.SDK_INT >= 26) {
            vibrator.vibrate(VibrationEffect.createOneShot(duration, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            vibrator.vibrate(duration);
        }
    }

}
