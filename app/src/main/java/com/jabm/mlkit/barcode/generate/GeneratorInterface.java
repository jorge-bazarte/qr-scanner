package com.jabm.mlkit.barcode.generate;

import android.graphics.Bitmap;

public interface GeneratorInterface {
    void onCodeCreated(Bitmap bitmap);

    void onCodeError(Exception e);
}
