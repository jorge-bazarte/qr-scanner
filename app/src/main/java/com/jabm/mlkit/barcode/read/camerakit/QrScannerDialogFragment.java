package com.jabm.mlkit.barcode.read.camerakit;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetector;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetectorOptions;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.jabm.mlkit.barcode.R;
import com.jabm.mlkit.barcode.read.ScanResultInterface;
import com.wonderkiln.camerakit.CameraView;

import static android.app.Activity.RESULT_OK;

public class QrScannerDialogFragment extends AppCompatDialogFragment {

    public static String TAG = "QrScannerDialogFragment";
    public static final int STORAGE_REQUEST_PERMISSION = 13;
    private CameraView cameraPreview;
    private ScanResultInterface scanResultInterface;

    @SuppressLint("ValidFragment")
    private QrScannerDialogFragment() {
    }

    public static QrScannerDialogFragment newInstance() {
        QrScannerDialogFragment f = new QrScannerDialogFragment();
        Bundle args = new Bundle();
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_FullScreenDialogStyle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_fragment_qr_scanner, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        cameraPreview = view.findViewById(R.id.camera_preview);
        view.findViewById(R.id.button_gallery).setOnClickListener(v -> pickImageFromGallery());
        view.findViewById(R.id.button_scan).setOnClickListener(v -> {
            cameraPreview.captureImage(cameraKitImage -> getQRCodeDetails(cameraKitImage.getBitmap()));
            Log.d("QR", "onResume");
        });
        view.findViewById(R.id.close_dialog).setOnClickListener(v -> {
            dismiss();
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        cameraPreview.start();
    }

    @Override
    public void onPause() {
        cameraPreview.stop();
        super.onPause();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case STORAGE_REQUEST_PERMISSION:
                pickImageFromGallery();
                break;
            default:
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                Uri uri = data.getData();
                if (scanResultInterface != null) {
                    dismiss();
                    scanResultInterface.onScanResult(uri.toString());
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                    getQRCodeDetails(bitmap);
                }
            }
        } catch (Exception e) {
            Log.e("FileSelectorActivity", "File select error", e);
        }
    }

    private void getQRCodeDetails(Bitmap bitmap) {
        Log.d("QR", "getQRCodeDetails");
        FirebaseVisionBarcodeDetectorOptions options = new FirebaseVisionBarcodeDetectorOptions.Builder()
                .setBarcodeFormats(FirebaseVisionBarcode.FORMAT_ALL_FORMATS)
                .build();
        FirebaseVisionBarcodeDetector detector = FirebaseVision.getInstance().getVisionBarcodeDetector(options);
        FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmap);
        detector.detectInImage(image)
                .addOnSuccessListener(bs -> {
            Log.d("QR", "Scan barcodes " + bs.toString());
            for (FirebaseVisionBarcode fbBarcode : bs) {
                Log.d("QR", "" +
                        " " + fbBarcode.toString());
                if (scanResultInterface != null) {
                    scanResultInterface.onScanResult(fbBarcode.getRawValue());
                    dismiss();
                }
            }
        })
                .addOnFailureListener(e -> {
                    if (scanResultInterface != null) {
                        scanResultInterface.onError(e.getMessage());
                        dismiss();
                    }
                });
    }

    private void pickImageFromGallery() {
        if (isStoragePermissionsGranted()) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), 123);
        } else {
            getStoragePermissions();
        }
    }


    private boolean isStoragePermissionsGranted() {
        return (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private void getStoragePermissions() {
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_REQUEST_PERMISSION);
    }

    public void addScanResultListener(ScanResultInterface scanResultInterface) {
        this.scanResultInterface = scanResultInterface;
    }
}
