package com.jabm.mlkit.barcode.read;

public interface ScanResultInterface {
    void onScanResult(String s);

    void onError(String e);
}
