package com.jabm.mlkit.barcode.read.zxing;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.zxing.Result;
import com.jabm.mlkit.barcode.R;
import com.jabm.mlkit.barcode.common.VibrateHelper;
import com.jabm.mlkit.barcode.read.ScanResultInterface;

import java.io.IOException;

import static android.app.Activity.RESULT_OK;

public class QrScannerZxingDialogFragment extends AppCompatDialogFragment implements BarcodeZxingScanningProcessor.OnScannResult {

    public static final int CAMERA_REQUEST_PERMISSION = 12;
    public static final int STORAGE_REQUEST_PERMISSION = 13;

    private ScanResultInterface scanResultInterface;
    private CameraSource cameraSource = null;
    private CameraSourcePreview preview;
    private BarcodeZxingScanningProcessor barcodeScanningProcessor;
    private VibrateHelper vibrateHelper;

    public static QrScannerZxingDialogFragment newInstance() {
        QrScannerZxingDialogFragment f = new QrScannerZxingDialogFragment();
        Bundle args = new Bundle();
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_FullScreenDialogStyle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_fragment_qr_scanner_zxing, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        vibrateHelper = new VibrateHelper(getActivity());
        initViews(view);
        initZxingVision();
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isCameraPermissionsGranted()) {
            startCameraSource();
        } else {
            getCameraPermissions();
        }
    }

    @Override
    public void onPause() {
        preview.stop();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        if (cameraSource != null) {
            cameraSource.release();
        }
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CAMERA_REQUEST_PERMISSION:
                if (isCameraPermissionsGranted()) {
                    startCameraSource();
                } else {
                    dismiss();
                }
                break;
            case STORAGE_REQUEST_PERMISSION:
                pickImageFromGallery();
                break;
            default:
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                Uri uri = data.getData();
                if (scanResultInterface != null) {
                    dismiss();
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                    barcodeScanningProcessor.process(bitmap);
                }
            }
        } catch (Exception e) {
            Log.e("FileSelectorActivity", "File select error", e);
        }
    }

    @Override
    public void onBarcode(Result result) {
        vibrateHelper.vibrate(VibrateHelper.MEDIUM_DURATION);
        if (scanResultInterface != null) {
            dismiss();
            scanResultInterface.onScanResult(result.getText());
        }
    }

    private void initViews(View view) {
        preview = view.findViewById(R.id.camera_preview);
        view.findViewById(R.id.close_dialog).setOnClickListener(v -> dismiss());
        view.findViewById(R.id.button_gallery).setOnClickListener(v -> pickImageFromGallery());
    }

    private void initZxingVision() {
        barcodeScanningProcessor = new BarcodeZxingScanningProcessor(this);
        createCameraSource();
    }

    private boolean isCameraPermissionsGranted() {
        return (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED);
    }

    private void getCameraPermissions() {
        requestPermissions(new String[]{Manifest.permission.CAMERA}, CAMERA_REQUEST_PERMISSION);
    }

    private boolean isStoragePermissionsGranted() {
        return (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private void getStoragePermissions() {
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_REQUEST_PERMISSION);
    }

    private void createCameraSource() {
        if (cameraSource == null) {
            cameraSource = new CameraSource(getActivity());
        }
        cameraSource.setMachineLearningFrameProcessor(barcodeScanningProcessor);
    }

    private void startCameraSource() {
        if (cameraSource != null) {
            try {
                preview.start(cameraSource);
            } catch (IOException e) {
                cameraSource.release();
                cameraSource = null;
            }
        }
    }

    private void pickImageFromGallery() {
        if (isStoragePermissionsGranted()) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), 123);
        } else {
            getStoragePermissions();
        }
    }

    public void addScanResultListener(ScanResultInterface scanResultInterface) {
        this.scanResultInterface = scanResultInterface;
    }
}
