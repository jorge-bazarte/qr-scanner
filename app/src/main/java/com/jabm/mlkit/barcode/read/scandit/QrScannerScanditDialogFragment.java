package com.jabm.mlkit.barcode.read.scandit;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.jabm.mlkit.barcode.R;
import com.jabm.mlkit.barcode.read.ScanResultInterface;
import com.scandit.barcodepicker.BarcodePicker;
import com.scandit.barcodepicker.ProcessFrameListener;
import com.scandit.barcodepicker.ScanOverlay;
import com.scandit.barcodepicker.ScanSession;
import com.scandit.barcodepicker.ScanSettings;
import com.scandit.recognition.Barcode;

import static android.app.Activity.RESULT_OK;


public class QrScannerScanditDialogFragment extends AppCompatDialogFragment implements ProcessFrameListener {

    public static final int CAMERA_REQUEST_PERMISSION = 12;
    public static final int STORAGE_REQUEST_PERMISSION = 13;
    private ScanResultInterface scanResultInterface;
    private FrameLayout previewContainer;
    private BarcodePicker mBarcodePicker;

    public static QrScannerScanditDialogFragment newInstance() {
        QrScannerScanditDialogFragment f = new QrScannerScanditDialogFragment();
        Bundle args = new Bundle();
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.AppTheme_FullScreenDialogStyle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_fragment_qr_scanner_scandit, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(view);
        initializeAndStartBarcodeScanning();
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isCameraPermissionsGranted()) {
            mBarcodePicker.startScanning();
        } else {
            getCameraPermissions();
        }
    }

    @Override
    public void onPause() {
        mBarcodePicker.stopScanning();
        super.onPause();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CAMERA_REQUEST_PERMISSION:
                if (isCameraPermissionsGranted()) {
                    mBarcodePicker.startScanning();
                } else {
                    dismiss();
                }
                break;
            case STORAGE_REQUEST_PERMISSION:
                pickImageFromGallery();
                break;
            default:
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void didProcess(byte[] bytes, int i, int i1, ScanSession scanSession) {
        for (Barcode newBarcode : scanSession.getNewlyRecognizedCodes()) {
            dismiss();
            scanResultInterface.onScanResult(newBarcode.getData());
        }
    }

    private void initViews(View view) {
        previewContainer = view.findViewById(R.id.preview_container);
        view.findViewById(R.id.close_dialog).setOnClickListener(v -> dismiss());
        view.findViewById(R.id.button_gallery).setOnClickListener(v -> Toast.makeText(getContext(),"No disponible",Toast.LENGTH_SHORT).show());
    }

    private void initializeAndStartBarcodeScanning() {
        ScanSettings settings = ScanSettings.create();
        int[] symbologiesToEnable = new int[]{
                Barcode.SYMBOLOGY_EAN13,
                Barcode.SYMBOLOGY_EAN8,
                Barcode.SYMBOLOGY_UPCA,
                Barcode.SYMBOLOGY_CODE39,
                Barcode.SYMBOLOGY_CODE128,
                Barcode.SYMBOLOGY_INTERLEAVED_2_OF_5,
                Barcode.SYMBOLOGY_UPCE,
                Barcode.SYMBOLOGY_QR,
                Barcode.SYMBOLOGY_MICRO_QR
        };
        for (int sym : symbologiesToEnable) {
            settings.setSymbologyEnabled(sym, true);
        }
        settings.setMatrixScanEnabled(true);
        settings.setMaxNumberOfCodesPerFrame(1);
        settings.setCodeRejectionEnabled(true);
        settings.setCameraFacingPreference(ScanSettings.CAMERA_FACING_BACK);
        boolean emulatePortraitMode = !BarcodePicker.canRunPortraitPicker();
        if (emulatePortraitMode) {
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
        mBarcodePicker = new BarcodePicker(getContext(), settings);
        mBarcodePicker.getOverlayView().setGuiStyle(ScanOverlay.GUI_STYLE_NONE);
        mBarcodePicker.getOverlayView().setTorchEnabled(false);
        mBarcodePicker.getOverlayView().setVibrateEnabled(true);
        mBarcodePicker.setProcessFrameListener(this);

        previewContainer.addView(mBarcodePicker.getRootView());
    }

    private boolean isCameraPermissionsGranted() {
        return (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED);
    }

    private void getCameraPermissions() {
        requestPermissions(new String[]{Manifest.permission.CAMERA}, 1);
    }

    private boolean isStoragePermissionsGranted() {
        return (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }

    private void getStoragePermissions() {
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_REQUEST_PERMISSION);
    }

    private void pickImageFromGallery() {
        if (isStoragePermissionsGranted()) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), 123);
        } else {
            getStoragePermissions();
        }
    }

    public void addScanResultListener(ScanResultInterface scanResultInterface) {
        this.scanResultInterface = scanResultInterface;
    }
}
