// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package com.jabm.mlkit.barcode.read.zxing;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.NotFoundException;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.qrcode.QRCodeReader;

public class BarcodeZxingScanningProcessor extends VisionProcessorBase<Result> {

    private static final String TAG = "BarcodeScanProc";
    private OnScannResult onScannResult;

    public BarcodeZxingScanningProcessor(OnScannResult onScannResult) {
        this.onScannResult = onScannResult;
    }

    @Override
    public void stop() {

    }

    @Override
    protected Result detectInImage(BinaryBitmap bitmap) throws FormatException, ChecksumException, NotFoundException {
        Reader reader = new QRCodeReader();
        return reader.decode(bitmap);
    }

    @Override
    protected void onSuccess(@NonNull Result result) {
        if (onScannResult != null) {
            onScannResult.onBarcode(result);
        }
    }

    @Override
    protected void onFailure(@NonNull Exception e) {
        Log.e(TAG, "Barcode detection failed " + e);
    }

    public interface OnScannResult {
        void onBarcode(Result result);
    }
}
