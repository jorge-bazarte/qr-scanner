package com.jabm.mlkit.barcode.generate;

import android.graphics.Bitmap;
import android.graphics.Color;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

public class ZxingGenerator {
    private GeneratorInterface generatorInterface;
    private int qrSize = 500;
    private BarcodeFormat barcodeFormat = BarcodeFormat.QR_CODE;

    public ZxingGenerator(GeneratorInterface generatorInterface) {
        this.generatorInterface = generatorInterface;
    }

    public void generate(String rawTest) {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        try {
            if (!rawTest.isEmpty())
                generatorInterface.onCodeCreated(bitMatrixToBitMap(qrCodeWriter.encode(rawTest, barcodeFormat, qrSize, qrSize)));
        } catch (WriterException e) {
            generatorInterface.onCodeError(e);
        }
    }

    public void setQrSize(int qrSize) {
        this.qrSize = qrSize;
    }

    public void setBarcodeFormat(BarcodeFormat barcodeFormat) {
        this.barcodeFormat = barcodeFormat;
    }

    private Bitmap bitMatrixToBitMap(BitMatrix bitMatrix) {
        int w = bitMatrix.getWidth();
        int h = bitMatrix.getHeight();
        int[] rawData = new int[w * h];
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                int color = Color.WHITE;
                if (bitMatrix.get(i, j)) {
                    color = Color.BLACK;
                }
                rawData[i + (j * w)] = color;
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.RGB_565);
        bitmap.setPixels(rawData, 0, w, 0, 0, w, h);
        return bitmap;
    }
}
