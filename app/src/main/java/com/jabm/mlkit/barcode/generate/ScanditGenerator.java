package com.jabm.mlkit.barcode.generate;

import android.content.Context;
import android.graphics.Bitmap;

import com.scandit.barcodepicker.BarcodePicker;
import com.scandit.barcodepicker.ScanditLicense;
import com.scandit.generator.BarcodeGenerator;
import com.scandit.recognition.Barcode;

import java.util.HashMap;

public class ScanditGenerator {
    private final String scanditSdkKey = "AXBs4j2hIUINN103cBOsgjQPNEdxOLGw62KN/eBYkxutH6A723uD4v0sSBJ2RbS/8m6augBuOh10BsGVEX48Sd1t728XaXA/9jAwI+x78kBUfvJVEkIYhsVi50rKW8cPTgquGGAEwE7HO2gv8c6253R82VoITwabSFO9ig1DqjHw3BOwC/GJdvCClA8iXthpaoeo92OfKzwG+eyVRYpmRgVxA/tyeRLITJBrWlTCnViLXgNn5oTfryDaql6uKiy7XUv3qZAtDlmIOMI1J2RMpCuMsDUsia/2cWEuxXBRHjol5mcqONsZ+uDLNAaWd2NdC6wYkhhA+wfTcUKDmDyVr6YNC/8H1nJzM3j1ETpBdJv/ZZl0NGTfWt3bGklUkDa1AgTAJNv0wGSczfA0/YUaMLXEGL23BG7M7EQsptFVhI17sCAwF7tUjs9zKRA5kZVI2naF4a5LEBNY9tJucWQP1tm4sjtzBz9sLFKbUthwelYPQldQGQrQ756L4U78njjY5tAHM6BUi0Nc1Rvf2xbAeUpmLBEeC7CH96LegqgQw899uk8LV+4J+b9bDLI9/p7PzYdjoRfk0ZTNZgtA5CelmoV8xP6IO7CK2NgfmL0vIgTziZRO4iSiuD4SDTd+3sDZ/aP7wW0/vMCffwphtuWjzTSBmvz3nR4eANq6BRLmYjTKHKWQ3pxDbEMaW0dEY0n8jn8OCAGR09EU/v+sAeRtfB4h0MSnUKzeUWoYh+X3ywPoFv6TOMe7n5K4xd5F4nj+onkrd43cUHt5w8uMicYaHDQ1Q6qo+7rNugyV8+402xrf1FHbXemn/Bc=";

    private GeneratorInterface generatorInterface;
    private BarcodePicker barcodePicker;
    private int qrSize = 500;
    private int[] foregroundColor = {0, 0, 0, 255};
    private int[] backgroundColor = {255, 255, 255, 255};
    private int symbology = Barcode.SYMBOLOGY_QR;

    public ScanditGenerator(Context context, GeneratorInterface generatorInterface) {
        ScanditLicense.setAppKey(scanditSdkKey);
        this.generatorInterface = generatorInterface;
        barcodePicker = new BarcodePicker(context);
    }

    public void generate(String rawTest) {
        if (!rawTest.isEmpty()) {
            try {
                BarcodeGenerator generator = barcodePicker.createBarcodeGeneratorForSymbology(symbology);
                HashMap<String, Object> options = new HashMap<>();
                options.put("foregroundColor", foregroundColor);
                options.put("backgroundColor", backgroundColor);
                generator.setOptions(options);
                Bitmap image = generator.generate(rawTest);
                float factor = (image.getWidth() > image.getHeight()) ? qrSize / image.getWidth() : qrSize / image.getHeight();
                Bitmap imageScaled = Bitmap.createScaledBitmap(image, (int) (image.getWidth() * factor), (int) (image.getHeight() * factor), false);
                generatorInterface.onCodeCreated(imageScaled);
            } catch (Exception e) {
                generatorInterface.onCodeError(e);
            }
        }
    }

    public void setQrSize(int qrSize) {
        this.qrSize = qrSize;
    }

    public void setForegroundColor(int[] foregroundColor) {
        this.foregroundColor = foregroundColor;
    }

    public void setBackgroundColor(int[] backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public void setSymbology(int symbology) {
        this.symbology = symbology;
    }
}
