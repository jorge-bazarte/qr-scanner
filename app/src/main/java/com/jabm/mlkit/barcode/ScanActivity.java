package com.jabm.mlkit.barcode;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.jabm.mlkit.barcode.generate.GeneratorInterface;
import com.jabm.mlkit.barcode.generate.ScanditGenerator;
import com.jabm.mlkit.barcode.generate.ZxingGenerator;
import com.jabm.mlkit.barcode.read.ScanResultInterface;
import com.jabm.mlkit.barcode.read.camerakit.QrScannerDialogFragment;
import com.jabm.mlkit.barcode.read.firebase.QrScannerFBDialogFragment;
import com.jabm.mlkit.barcode.read.libcodescanner.QrScannerCsDialogFragment;
import com.jabm.mlkit.barcode.read.scandit.QrScannerScanditDialogFragment;
import com.jabm.mlkit.barcode.read.zxing.QrScannerZxingDialogFragment;

public class ScanActivity extends AppCompatActivity implements GeneratorInterface, ScanResultInterface {
    private ZxingGenerator zxingGenerator;
    private ScanditGenerator scanditGenerator;

    private ImageView imageQR;
    private EditText textRaw;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        initToolbar();

        zxingGenerator = new ZxingGenerator(this);
        scanditGenerator = new ScanditGenerator(this, this);

        initViews();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_scan, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_scan_zxing:
                showDialogZxing();
                break;
            case R.id.action_scan_scandit:
                showDialogScandit();
                break;
            case R.id.action_scan_firebase:
                showDialogFb();
                break;
            case R.id.action_scan_cdsc:
                showDialogCs();
                break;
            case R.id.action_scan_ck_fb:
                showDialogCkFb();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCodeCreated(Bitmap bitmap) {
        runOnUiThread(() -> imageQR.setImageBitmap(bitmap));
    }

    @Override
    public void onCodeError(Exception e) {
        runOnUiThread(() -> {
            textRaw.setText("");
            imageQR.setImageResource(R.drawable.ic_launcher_foreground);
            Toast.makeText(ScanActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
        });
    }

    @Override
    public void onScanResult(String s) {
        textRaw.setText(s);
        zxingGenerator.generate(textRaw.getText().toString());
    }

    @Override
    public void onError(String e) {
        textRaw.setText("Error!");
    }

    private void initToolbar() {
        setSupportActionBar(findViewById(R.id.toolbar));
    }

    private void initViews() {
        imageQR = findViewById(R.id.image_qr);
        textRaw = findViewById(R.id.raw_text);
        findViewById(R.id.button_qr_1).setOnClickListener(v -> zxingGenerator.generate(textRaw.getText().toString()));
        findViewById(R.id.button_qr_2).setOnClickListener(v -> scanditGenerator.generate(textRaw.getText().toString()));
    }

    private void showDialogCkFb() {
        QrScannerDialogFragment dialog = QrScannerDialogFragment.newInstance();
        dialog.addScanResultListener(this);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        dialog.show(ft, QrScannerDialogFragment.TAG);
    }

    private void showDialogFb() {
        QrScannerFBDialogFragment dialog = QrScannerFBDialogFragment.newInstance();
        dialog.addScanResultListener(this);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        dialog.show(ft, QrScannerDialogFragment.TAG);
    }

    private void showDialogCs() {
        QrScannerCsDialogFragment dialog = QrScannerCsDialogFragment.newInstance();
        dialog.addScanResultListener(this);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        dialog.show(ft, QrScannerDialogFragment.TAG);
    }

    private void showDialogScandit() {
        QrScannerScanditDialogFragment dialog = QrScannerScanditDialogFragment.newInstance();
        dialog.addScanResultListener(this);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        dialog.show(ft, QrScannerDialogFragment.TAG);
    }

    private void showDialogZxing() {
        QrScannerZxingDialogFragment dialog = QrScannerZxingDialogFragment.newInstance();
        dialog.addScanResultListener(this);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        dialog.show(ft, QrScannerDialogFragment.TAG);
    }

}
